package sorts

import (
	"math/rand"
	"time"
)

// Generates a slice of size, size filled with random numbers
func generateSlice(size int) []int {
	slice := make([]int, size, size)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		slice[i] = rand.Intn(size)
	}
	return slice
}

func ShellSort(arr []int) {
	for gap := len(arr) / 2; gap > 0; gap /= 2 {
		for i := gap; i < len(arr); i++ {
			for n := i; n >= gap && arr[n-gap] > arr[n]; n -= gap {
				a := arr[n]
				arr[n] = arr[n-gap]
				arr[n-gap] = a
			}
		}
	}
}

func InsertionSort(arr []int) {
	for i := 1; i < len(arr); i++ {
		for n := i; n > 0 && arr[n-1] > arr[n]; n-- {
			a := arr[n]
			arr[n] = arr[n-1]
			arr[n-1] = a
		}
	}
}


func SelectionSort(arr []int) {
	var min, index int
	for i := 0; i < len(arr); i++ {
		min = arr[i]
		for n := i; n < len(arr); n++ {
			if min > arr[n] {
				min = arr[n]
				index = n
			}
		}
		arr[index] = arr[i]
		arr[i] = min
	}
}

