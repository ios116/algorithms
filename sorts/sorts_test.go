package sorts

import (
	"sort"
	"testing"
)

const count = 1000

func TestShellSort(t *testing.T) {
	arr := generateSlice(10)
	t.Log(arr)
	ShellSort(arr)
	t.Log(arr)
	if !checkIsSort(arr) {
		t.Fatal("sort is fail")
	}
}

func TestInsertionSort(t *testing.T) {
	arr := generateSlice(10)
	InsertionSort(arr)
	if !checkIsSort(arr) {
		t.Fatal("sort is fail")
	}
}

func TestSelectionSort(t *testing.T) {
	arr := generateSlice(10)
	SelectionSort(arr)
	if !checkIsSort(arr) {
		t.Fatal("sort is fail")
	}
}

func BenchmarkInsertionSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		InsertionSort(arr)
	}
}

func BenchmarkShellSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		ShellSort(arr)
	}
}

func BenchmarkSelectionSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		SelectionSort(arr)
	}
}

func BenchmarkQuickSort(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		s:=NewQuickSort(arr)
		s.Sort(0,len(arr)-1)
	}
}

func BenchmarkHeapSort(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		s:=NewHeap(arr)
		s.Sort()
	}
}

func BenchmarkMergeSort(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		s:=NewMergeSort(arr)
		s.Sort(0,len(arr)-1)
	}
}

func BenchmarkStandard(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		arr := generateSlice(count)
		b.StartTimer()
		sort.Ints(arr)
	}
}


func checkIsSort(arr []int) bool {
	for i := 1; i < len(arr); i++ {
		if arr[i] < arr[i-1] {
			return false
		}
	}
	return true
}
