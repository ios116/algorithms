package sorts

type Heap struct {
	arr []int
}
func NewHeap(arr []int) *Heap {
	return &Heap{arr: arr}
}

func (h *Heap) swap(i, j int) {
	h.arr[i], h.arr[j] = h.arr[j], h.arr[i]
}

func (h *Heap) down(root, size int) {
	l := root*2 + 1
	r := l + 1
	x := root
	if l < size && h.arr[l] > h.arr[x] {
		x = l
	}
	if r < size && h.arr[r] > h.arr[x] {
		x = r
	}
	if x == root {
		return
	}
	h.swap(x, root)
	h.down(x, size)
}

func (h *Heap) Sort() {
	root := len(h.arr)/2-1
	for i:=root; i>=0; i-- {
		h.down(i,len(h.arr))
	}
	for i:=len(h.arr)-1; i>=0; i-- {
		h.swap(0,i)
		h.down(0,i)
	}
}