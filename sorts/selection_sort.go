package sorts

type Selection struct {
	arr []int
}

func NewSelection(arr []int) *Selection {
	return &Selection{arr: arr}
}

func (s *Selection) Swap(a, b int) {
	s.arr[a], s.arr[b] = s.arr[b], s.arr[a]
}

func (s *Selection) MoveMaxToRoot(root int, size int) {
	for i := root+1; i < size; i++ {
		if s.arr[root] < s.arr[i] {
			s.Swap(root, i)
		}
	}
}

func (s *Selection) Sort() {
	s.MoveMaxToRoot(0,len(s.arr))
	for i:=len(s.arr)-1; i >= 0; i-- {
		s.Swap(0,i)
		s.MoveMaxToRoot(0,i)
	}
}
