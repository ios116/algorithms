package sorts

type QuickSort struct {
	arr []int
}

func NewQuickSort(arr []int) *QuickSort {
	return &QuickSort{arr: arr}
}

// swap меняем местами
func (q *QuickSort) swap(a, b int) {
	x := q.arr[a]
	q.arr[a] = q.arr[b]
	q.arr[b] = x
}

func (q *QuickSort) partition(l, r int) int {
	pivot:=q.arr[r]
	a := l-1
	for i := l; i <= r; i++ {
		if q.arr[i] <= pivot {
			a++
			q.swap(a, i)
		}
	}
	return a
}

func (q *QuickSort) Sort(l, r int)  {
    if l >= r {return}
    a := q.partition(l,r)
    q.Sort(l,a-1)
    q.Sort(a+1,r)
}
