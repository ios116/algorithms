package sorts

type MergeSort struct {
	arr []int
}

func NewMergeSort(arr []int) *MergeSort {
	return &MergeSort{arr: arr}
}

func (m *MergeSort) Merge(a, b []int) []int {
	var aIndex, bIndex int
	result := make([]int, 0, len(a)+len(b))
	for aIndex < len(a) && bIndex < len(b) {
		if a[aIndex] < b[bIndex] {
			result = append(result, a[aIndex])
			aIndex++
		} else {
			result = append(result, b[bIndex])
			bIndex++
		}
	}
	if aIndex < len(a) {
		result = append(result, a[aIndex:]...)
	}
	if bIndex < len(b) {
		result = append(result, b[bIndex:]...)
	}

	return result
}

func (m *MergeSort) Merge2(l,c,r int) {
	aIndex := l
	bIndex := c+1
	result := make([]int, 0, r-l+1)
	for aIndex <= c && bIndex <= r {
		if m.arr[aIndex] < m.arr[bIndex] {
			result = append(result, m.arr[aIndex])
			aIndex++
		} else {
			result = append(result, m.arr[bIndex])
			bIndex++
		}
	}
	if aIndex < c+1 {
		result = append(result, m.arr[aIndex:c+1]...)
	}
	if bIndex < r {
		result = append(result, m.arr[bIndex:r+1]...)
	}

	for i:=0; i<len(result); i++ {
		m.arr[l+i]=result[i]
	}

}

func (m *MergeSort) Sort(l, r int) {
	if l >= r {
		return
	}
	c := l+(r - l) / 2
	m.Sort(l, c)
	m.Sort(c+1, r)
	m.Merge2(l,c,r)
}
